
/**
 * Imports
 */
const express = require('express');
const bodyParser = require('body-parser');
const firebase = require('firebase');
const mm = require('music-metadata');
const admin = require('firebase-admin');
const md5 = require('md5');
const sharp = require('sharp');
const fs = require('fs');
const path = require('path');
const https = require('https');
const axios = require('axios');
const { config } = require('./firebase/config');
const { settings } = require('./settings');

/**
 * Setup Firebase
 */

firebase.initializeApp(config);

admin.initializeApp({
  credential: admin.credential.applicationDefault(),
  databaseURL: 'https://fillydelphia-radio.firebaseio.com',
});

const db = admin.database();
const ref = db.ref('rest/fillydelphia-radio/now-playing');

/**
 * Variables
 */
const app = express();

/**
 * Express Middleware
 */

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

/**
 * FUNCTIONS
 */

function getCoversFromFile(filename) {
  return mm.parseFile(filename).then(data => data);
}

function calculateHash(data) {
  return md5(data);
}

function checkAudioExists(file) {
  return fs.existsSync(file);
}

function checkFileExists(hash) {
  return fs.existsSync(`${settings.output}${hash}.jpg`);
}

function generateImages(image, hash) {
  return Promise.all([
    sharp(image)
      .resize(96)
      .jpeg({ quality: 90 })
      .toFile(`${settings.output}${hash}-96.jpg`),
    sharp(image)
      .resize(128)
      .jpeg({ quality: 90 })
      .toFile(`${settings.output}${hash}-128.jpg`),
    sharp(image)
      .resize(192)
      .jpeg({ quality: 90 })
      .toFile(`${settings.output}${hash}-192.jpg`),
    sharp(image)
      .resize(256)
      .jpeg({ quality: 90 })
      .toFile(`${settings.output}${hash}-256.jpg`),
    sharp(image)
      .resize(384)
      .jpeg({ quality: 90 })
      .toFile(`${settings.output}${hash}-384.jpg`),
    sharp(image)
      .resize(512)
      .jpeg({ quality: 90 })
      .toFile(`${settings.output}${hash}-512.jpg`),
    sharp(image)
      .jpeg({ quality: 90 })
      .toFile(`${settings.output}${hash}.jpg`),
  ])
    .then(() => true)
    .catch(() => false);
}

function coverList(hash) {
  return {
    96: `${settings.imgurl}${hash}-96.jpg`,
    128: `${settings.imgurl}${hash}-128.jpg`,
    192: `${settings.imgurl}${hash}-192.jpg`,
    256: `${settings.imgurl}${hash}-256.jpg`,
    384: `${settings.imgurl}${hash}-384.jpg`,
    512: `${settings.imgurl}${hash}-512.jpg`,
    original: `${settings.imgurl}${hash}.jpg`,
  };
}

// eslint-disable-next-line consistent-return
async function getCovers(filename) {
  const exists = await checkAudioExists(filename);
  if (exists) {
    const data = await getCoversFromFile(filename);
    if (!data.common.picture) {
      return false;
      // eslint-disable-next-line no-else-return
    } else {
      const image = data.common.picture[0].data;
      const hash = await calculateHash(image);
      const doesExist = await checkFileExists(hash);
      if (!doesExist) {
        await generateImages(image, hash);
        return coverList(hash);
        // eslint-disable-next-line no-else-return
      } else {
        return coverList(hash);
      }
    }
  } else {
    return false;
  }
}

/**
 * Santizes Metadata, returning a subset of the metadata for public use
 * @param {object} allMetadata
 * @returns {object} subset of allMetadata
 */

function sanitizeMetadata(allMetadata) {
  return (({
    album = 'Unknown Album',
    albumartist = 'Unknown Album Artist',
    artist = 'Unknown Artist',
    comment = '""',
    covers,
    date = '0',
    on_air = '0',
    title = 'Unknown Song',
    unsyncedlyrics = 'none',
    year = '0',
    store = 'none',
    wwwpublisher = 'none',
    wwwaudiosource = 'none',
    wwwartist = 'none',
    wwwcopyright = 'none',
    wwwpayment = 'none',
    www = 'none',
    isLive,
    listeners,
  }) => ({
    album,
    albumartist,
    artist,
    comment,
    covers,
    date,
    on_air,
    title,
    unsyncedlyrics,
    year,
    store,
    wwwpublisher,
    wwwaudiosource,
    wwwartist,
    wwwcopyright,
    wwwpayment,
    www,
    isLive,
    listeners,
  }))(allMetadata);
}

function sanitizeDJMetadata(allMetadata) {
  return (({
    icename = 'Unavailable',
    artist = 'Unavailable',
    title = 'Unavailable',
    description = 'Unavailable',
    covers,
    isLive,
    listeners,
  }) => ({
    icename,
    artist,
    title,
    description,
    covers,
    isLive,
    listeners,
  }))(allMetadata);
}

/**
 *
 * Does sign in for DJ
 *
 * @param {string} user Username
 * @param {string} password Password
 * @param {Response} res Response
 */

function doSignInForDJ(user, password, res) {
  firebase
    .auth()
    .signInWithEmailAndPassword(user, password)
    .then(() => {
      res.send('true');
    })
    .catch(() => {
      res.send('false');
    });
}

async function updatePublicMetadata(publicMetadata) {
  const updated = await ref.set(publicMetadata);
  https.get(
    `https://air.radiotime.com/Playing.ashx?partnerId=xxxxx&partnerKey=xxxxx&id=xxxxx&title=${publicMetadata.title}&artist=${publicMetadata.artist}`,
  );
  return updated;
}

async function processAutoDJMeta(req) {
  const allMetadata = req.body;
  // Experimental
  if (allMetadata.comment) {
    if (allMetadata.comment.includes('Visit ')) {
      const bcURL = allMetadata.comment.substring(6);
      allMetadata.wwwpublisher = bcURL;
      allMetadata.store = 'Bandcamp';
    }
  }
  if (allMetadata.filename) {
    const filename = path.parse(allMetadata.filename).base;
    allMetadata.filename = `${settings.musicDirectory}${filename}`;
    const covers = await getCovers(allMetadata.filename);
    if (covers) {
      allMetadata.covers = covers;
    } else {
      allMetadata.covers = settings.noCovers;
    }
  } else {
    allMetadata.covers = settings.noCovers;
  }
  await axios.get('https://fillyradio.com:8443/status-json.xsl').then((result) => {
    let a = 0;
    result.data.icestats.source.forEach((e) => {
      a += e.listeners;
    });
    allMetadata.listeners = a;
  });
  allMetadata.isLive = false;
  const publicMetadata = sanitizeMetadata(allMetadata);
  updatePublicMetadata(publicMetadata);
}

async function processLiveDJMeta(req) {
  const allMetadata = req.body;
  const fixedMetadata = {};

  await axios.get('https://fillyradio.com:8443/status-json.xsl').then((result) => {
    let a = 0;
    result.data.icestats.source.forEach((e) => {
      a += e.listeners;
    });
    allMetadata.listeners = a;
  });

  Object.keys(allMetadata).forEach((key) => {
    const oldKey = key;
    const lowKey = oldKey.toLowerCase();
    const newKey = lowKey.replace('-', '');
    fixedMetadata[newKey] = allMetadata[key];
  });

  // We intend to have a database with shows, and have data pulled
  // accordingly, but for now we'll simply take the header info and
  // push that as metadata.

  if (!fixedMetadata.title) {
    if (!fixedMetadata.song) {
      fixedMetadata.song = 'Refreshing - Refreshing';
      fixedMetadata.title = 'Refreshing - Refreshing';
    } else {
      fixedMetadata.title = fixedMetadata.song;
    }
  }

  if (!fixedMetadata.artist) {
    const song = fixedMetadata.title;
    const songSplit = song.split(' - ');
    // eslint-disable-next-line prefer-destructuring
    fixedMetadata.artist = songSplit[0];
    // eslint-disable-next-line prefer-destructuring
    fixedMetadata.title = songSplit[1];
  }

  if (!fixedMetadata.icename) {
    fixedMetadata.icename = fixedMetadata.icedescription;
  }

  fixedMetadata.covers = settings.noCovers;
  fixedMetadata.isLive = true;

  const publicMetadata = sanitizeDJMetadata(fixedMetadata);
  updatePublicMetadata(publicMetadata);
}

/**
 * Routes
 */

app.post('/admin/update-metadata/:secret', async (req, res) => {
  if (req.params.secret !== settings.secret) {
    res.send('Fuck you');
  } else if (req.body['ice-name']) {
    res.send('Updating Metadata');
    await processLiveDJMeta(req);
  } else {
    res.send('Updating Metadata');
    await processAutoDJMeta(req);
  }
});

app.get('/admin/dj-logoff/:secret', (req, res) => {
  if (req.params.secret !== settings.secret) {
    res.send('Fuck you');
  } else {
    res.send('Signed Off');
  }
});

app.post('/admin/dj-login/:secret', (req, res) => {
  if (req.params.secret !== settings.secret) {
    res.send('Fuck you');
  } else {
    let { user } = req.body;
    let { password } = req.body;

    if (user === 'source') {
      const cred = password.split('|');
      // eslint-disable-next-line prefer-destructuring
      user = cred[0];
      // eslint-disable-next-line prefer-destructuring
      password = cred[1];
    }

    doSignInForDJ(user, password, res);
  }
});

// eslint-disable-next-line no-console
app.listen(settings.port, () => console.log(`Example app listening on port ${settings.port}!`));

const settings = {
  musicDirectory: '/path/to/music/',
  secret: 'secretlmao',
  port: 3000,
  noCovers: {
    96: 'https://res.cloudinary.com/stormdragon-designs/image/upload/s--2J7STH-X--/c_scale,w_96/v1526676139/fricon_600-600_zersgq.png',
    128: 'https://res.cloudinary.com/stormdragon-designs/image/upload/s--P6KcG8CW--/c_scale,w_128/v1526676139/fricon_600-600_zersgq.png',
    192: 'https://res.cloudinary.com/stormdragon-designs/image/upload/s--jjah7-bH--/c_scale,w_192/v1526676139/fricon_600-600_zersgq.png',
    256: 'https://res.cloudinary.com/stormdragon-designs/image/upload/s--jS8KMWIJ--/c_scale,w_256/v1526676139/fricon_600-600_zersgq.png',
    384: 'https://res.cloudinary.com/stormdragon-designs/image/upload/s--CbXyLUoX--/c_scale,w_384/v1526676139/fricon_600-600_zersgq.png',
    512: 'https://res.cloudinary.com/stormdragon-designs/image/upload/s--Z4rag5ok--/c_scale,w_512/v1526676139/fricon_600-600_zersgq.png',
    original: 'https://res.cloudinary.com/stormdragon-designs/image/upload/s--tSYzfM4R--/v1526676139/fricon_600-600_zersgq.png',
  },
  output: '/where/your/covers/are/served/from/',
  imgurl: 'https://url.of/covers/output',
};
exports.settings = settings;

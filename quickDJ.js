const admin = require('firebase-admin');

admin.initializeApp({
  credential: admin.credential.applicationDefault(),
  databaseURL: 'https://fillydelphia-radio.firebaseio.com',
});
const db = admin.database();
const ref = db.ref('rest/fillydelphia-radio/now-playing');
function addDJHeaders() {
  ref.once('value', (data) => {
    const a = data.val();
    a.isLive = true;
    a.icename = 'Testing Show with Set-L';
    ref.set(a);
  });
}
addDJHeaders();

// eslint-disable-next-line no-unused-vars
const traktor = {
  'Accept-Encoding': 'identity;q=0.5, gzip;q=1.0',
  'ice-url': 'Stream URL',
  Host: '192.168.88.124:8088',
  'ice-name': 'Traktor Stream',
  encoder: 'Native Instruments Media Library',
  Authorization:
    'Basic c291cmNlOndlc3RqQGZpbGx5ZGVscGhpYXJhZGlvLm5ldHxSNGluYm93RDRzaA==',
  'ice-description': 'Traktor Stream',
  'ice-public': '1',
  'User-Agent': 'Native Instruments IceCast Uplink',
  'ice-genre': 'Mixed Styles',
  artist: 'Loopmasters',
  Connection: 'Close',
  'content-type': 'application/ogg',
  'ice-bitrate': 'Quality 0',
  Accept: '*/*',
  title: 'House 2',
  vendor: 'Xiph.Org libVorbis I 20150105 (⛄⛄⛄⛄)',
  'ice-private': '0',
  'ice-audio-info': 'ice-samplerate=44100;ice-bitrate=Quality 0;ice-channels=2',
};

// eslint-disable-next-line no-unused-vars
const virtualDJICecast = {
  'ice-url': 'http://www.virtualdj.com/',
  'Content-Type': 'audio/mpeg',
  'ice-name': 'SESSION',
  Authorization: 'Basic d2VzdGpAZmlsbHlkZWxwaGlhcmFkaW8ubmV0OlI0aW5ib3dENHNo',
  'ice-public': '0',
  'ice-description': 'VirtualDJ Broadcast',
  song:
    'Above & Beyond feat. Alex Vargas - Blue Sky Action  (Above & Beyond Club Mix [Single])',
  'User-Agent': 'VirtualDJ',
  'ice-genre': 'GENRE',
  'ice-bitrate': '320',
  title:
    'Above & Beyond feat. Alex Vargas - Blue Sky Action  (Above & Beyond Club Mix [Single])',
};

// eslint-disable-next-line no-unused-vars
const butt = {
  'ice-url': 'url',
  'Content-Type': 'audio/mpeg',
  Host: '127.0.0.1:8088',
  'ice-name': 'Tester',
  Authorization:
    'Basic c291cmNlOndlc3RqQGZpbGx5ZGVscGhpYXJhZGlvLm5ldHxSNGluYm93RDRzaA==',
  'ice-public': '1',
  'ice-description': 'Description',
  song: 'Taps - ITS LIKE',
  'User-Agent': 'butt 0.1.17',
  'ice-genre': 'genre',
  title: 'Taps - ITS LIKE',
  'ice-audio-info': 'ice-bitrate=128;ice-channels=2;ice-samplerate=44100',
};

// eslint-disable-next-line no-unused-vars
const audiohijack = {
  'ice-url': 'URL Metadata',
  'ice-name': 'Test Broadcast',
  Authorization: 'Basic aWRvbnRleGlzdEBmdWNrLm1lOnNvbWV0aGluZ2lkaw==',
  'ice-public': '0',
  song: 'Axolotl - All Levels at Once ',
  'ice-genre': 'Genre Metadata',
  'content-type': 'audio/aac',
  'ice-bitrate': '320',
  title: 'Axolotl - All Levels at Once ',
};
